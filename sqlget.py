# working

import mysql.connector
from flask import Flask, jsonify, request, json
from flask_mysqldb import MySQL
from datetime import datetime
from flask_cors import CORS
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_jwt_extended import (create_access_token)

app = Flask(__name__)

mydb=mysql.connector.connect(host="localhost", user="root", passwd="", database="nodejs_login2")

app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
app.config['JWT_SECRET_KEY'] = 'secret'

mysql = MySQL(app)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)

CORS(app)


@app.route('/entroll/get/status', methods=['GET'])
def status():
    cur=mydb.cursor() 
    cur.execute("SELECT  status,input  FROM entroll where id=1" )
   
    result = cur.fetchall()
    for x in result:
	return (x)

@app.route('/entroll/get/input', methods=['GET'])
def input():
    cur=mydb.cursor() 
    cur.execute("SELECT  input,status  FROM entroll where id=1" )
   
    result = cur.fetchall()
    for x in result:
	return (x)


	
if __name__ == '__main__':
    app.run(debug=True)
